package com.mcm.banker.inventoryListeners;

import com.mcm.banker.hashs.isDepositing;
import com.mcm.banker.hashs.isWithdrawing;
import com.mcm.core.Main;
import com.mcm.core.database.CoinsDb;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryBanker implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        String uuid = player.getUniqueId().toString();

        if (event.getView().getTitle().equals(Main.getTradution("4xztjNMjm$qQzw5", uuid))) {
            event.setCancelled(true);

            //depositar
            if (event.getSlot() == 14) {
                if (isDepositing.get(uuid) != null) {
                    player.getOpenInventory().close();
                    player.sendMessage(Main.getTradution("?%6MVYp#52QhP4s", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    new isDepositing(uuid).insert();

                    player.getOpenInventory().close();
                    player.sendMessage(Main.getTradution("?%6MVYp#52QhP4s", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLaterAsynchronously(Main.plugin, () -> {
                        if (isDepositing.get(uuid) != null) isDepositing.get(uuid).delete();
                    }, 400L);
                }
            }

            //sacar
            if (event.getSlot() == 15) {
                if (CoinsDb.getCoins(uuid) <= 0) {
                    player.sendMessage(Main.getTradution("5Hk#kPyKjW44Utc", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                    return;
                }
                if (isWithdrawing.get(uuid) != null) {
                    player.getOpenInventory().close();
                    player.sendMessage(Main.getTradution("g7WAvdUcZr5WFX@", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                } else {
                    new isWithdrawing(uuid).insert();

                    player.getOpenInventory().close();
                    player.sendMessage(Main.getTradution("g7WAvdUcZr5WFX@", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);

                    Bukkit.getScheduler().runTaskLaterAsynchronously(Main.plugin, () -> {
                        if (isWithdrawing.get(uuid) != null) isWithdrawing.get(uuid).delete();
                    }, 400L);
                }
            }
        }
    }
}
