package com.mcm.banker.managers;

import com.mcm.core.Main;
import com.mcm.core.database.CoinsDb;
import com.mcm.core.utils.EnchantmentGlow;
import com.mcm.core.utils.RemoveAllFlags;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class InventoryBanker {

    public static DecimalFormat formatter = new DecimalFormat("#,###.00");

    public static Inventory get(String uuid) {
        Inventory inventory = Bukkit.createInventory(null, 3 * 9, Main.getTradution("4xztjNMjm$qQzw5", uuid));

        ItemStack saldo = new ItemStack(Material.MUSIC_DISC_BLOCKS);
        ItemMeta metaSaldo = saldo.getItemMeta();
        metaSaldo.setDisplayName(Main.getTradution("8Dh8?K!&WVC45kh", uuid));
        ArrayList<String> loreSaldo = new ArrayList<>();
        loreSaldo.add(" ");
        loreSaldo.add(Main.getTradution("Mc7kNwa9chhE#e9", uuid) + formatter.format(CoinsDb.getCoins(uuid)));
        loreSaldo.add(" ");
        metaSaldo.setLore(loreSaldo);
        saldo.setItemMeta(metaSaldo);

        ItemStack depositar = new ItemStack(Material.MUSIC_DISC_13);
        ItemMeta metaDepos = depositar.getItemMeta();
        metaDepos.setDisplayName(Main.getTradution("sHSqA372b@FP@zQ", uuid));
        ArrayList<String> loreDepos = new ArrayList<>();
        loreDepos.add(" ");
        loreDepos.add(Main.getTradution("$HAqbSZ!m6#*9D4", uuid));
        metaDepos.setLore(loreDepos);
        depositar.setItemMeta(metaDepos);

        ItemStack sacar = new ItemStack(Material.MUSIC_DISC_CAT);
        ItemMeta metaSacar = sacar.getItemMeta();
        metaSacar.setDisplayName(Main.getTradution("B@!NdR6ABEe5YNQ", uuid));
        ArrayList<String> loreSacar = new ArrayList<>();
        loreSacar.add(" ");
        loreSacar.add(Main.getTradution("Ah%5kReD#Qx%BAs", uuid));
        metaSacar.setLore(loreSacar);
        sacar.setItemMeta(metaSacar);

        inventory.setItem(11, EnchantmentGlow.addGlow(RemoveAllFlags.remove(saldo)));
        inventory.setItem(14, RemoveAllFlags.remove(depositar));
        inventory.setItem(15, RemoveAllFlags.remove(sacar));
        return inventory;
    }
}
