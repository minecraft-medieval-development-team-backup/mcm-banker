package com.mcm.banker.hashs;

import java.util.HashMap;

public class isDepositing {

    private String uuid;
    private static HashMap<String, isDepositing> cache = new HashMap<>();

    public isDepositing(String uuid) {
        this.uuid = uuid;
    }

    public isDepositing insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static isDepositing get(String uuid) {
        return cache.get(uuid);
    }

    public void delete() {
        this.cache.remove(this.uuid);
    }
}
