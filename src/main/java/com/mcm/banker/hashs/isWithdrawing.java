package com.mcm.banker.hashs;

import java.util.HashMap;

public class isWithdrawing {

    private String uuid;
    private static HashMap<String, isWithdrawing> cache = new HashMap<>();

    public isWithdrawing(String uuid) {
        this.uuid = uuid;
    }

    public isWithdrawing insert() {
        this.cache.put(this.uuid, this);
        return this;
    }

    public static isWithdrawing get(String uuid) {
        return cache.get(uuid);
    }

    public void delete() {
        this.cache.remove(this.uuid);
    }
}
