package com.mcm.banker.listeners;

import com.mcm.core.utils.BagCoinsUtil;
import com.mcm.core.utils.NBTTagUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class PickupItemEvent implements Listener {

    /**
     * Trying pickup the coins and added on inventory
     *
     * @param event
     */
    @EventHandler
    public void pickup(PlayerPickupItemEvent event) {
        Player player = event.getPlayer();

        if (event.getItem().getItemStack().getType().name().contains("SKULL") && NBTTagUtil.getIntData(event.getItem().getItemStack(), "coins") != 0 && BagCoinsUtil.haveSpaceOrBagCoins(player)) {
            BagCoinsUtil.addCoins(player, NBTTagUtil.getIntData(event.getItem().getItemStack(), "coins"));
            event.getItem().remove();
        }
    }
}
