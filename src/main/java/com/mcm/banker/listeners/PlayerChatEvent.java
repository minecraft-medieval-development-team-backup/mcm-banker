package com.mcm.banker.listeners;

import com.mcm.banker.hashs.isDepositing;
import com.mcm.banker.hashs.isWithdrawing;
import com.mcm.core.Main;
import com.mcm.core.database.CoinsDb;
import com.mcm.core.utils.BagCoinsUtil;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import java.text.DecimalFormat;

public class PlayerChatEvent implements Listener {

    public static DecimalFormat formatter = new DecimalFormat("#,###.00");

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onSpeak(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        String uuid = player.getUniqueId().toString();

        if (isDepositing.get(uuid) != null) {
            event.setCancelled(true);

            try {
                int value = Integer.valueOf(event.getMessage());

                if (BagCoinsUtil.removeCoins(player, value)) {
                    CoinsDb.updateCoins(uuid, CoinsDb.getCoins(uuid) + value);

                    player.sendMessage(Main.getTradution("Fv%zWAwcS7X&$45", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                    isDepositing.get(uuid).delete();
                } else {
                    player.sendMessage(Main.getTradution("aRuSt%8A3XcP#SU", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            } catch (NumberFormatException e) {
                player.sendMessage(Main.getTradution("5Z*8qY6?fNEY3#g", uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            }
        }

        if (isWithdrawing.get(uuid) != null) {
            event.setCancelled(true);

            try {
                int value = Integer.valueOf(event.getMessage());

                if (CoinsDb.getCoins(uuid) >= value) {
                    CoinsDb.updateCoins(uuid, CoinsDb.getCoins(uuid) - value);
                    BagCoinsUtil.addCoins(player, value);

                    player.sendMessage(Main.getTradution("s4s!RP73cN45Psj", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_YES, 1.0f, 1.0f);
                    isWithdrawing.get(uuid).delete();
                } else {
                    player.sendMessage(Main.getTradution("Uu3c#Xta3j!wTAy", uuid));
                    player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                    player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
                }
            } catch (NumberFormatException e) {
                player.sendMessage(Main.getTradution("5Z*8qY6?fNEY3#g", uuid));
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
                player.playSound(player.getLocation(), Sound.ENTITY_VILLAGER_NO, 1.0f, 1.0f);
            }
        }
    }
}
