package com.mcm.banker.listeners;

import com.mcm.banker.managers.InventoryBanker;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PlayerInteractBanker implements Listener {

    private static int npcID = 0;

    @EventHandler
    public void onClick(NPCRightClickEvent event) {
        Player player = event.getClicker();
        String uuid = player.getUniqueId().toString();

        if (event.getNPC().getId() == npcID) {
            player.openInventory(InventoryBanker.get(uuid));
            player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1.0f, 1.0f);
        }
    }
}
