package com.mcm.banker.listeners;

import com.mcm.core.utils.BagCoinsUtil;
import com.mcm.core.utils.NBTTagUtil;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryMoveItemEvent;

public class ItemMoveOnInventoryEvent implements Listener {

    /**
     * Making sure there is no more than one bag of coins if it is moved from inv to inv
     *
     * @param event
     */
    @EventHandler
    public void changeInventory(InventoryMoveItemEvent event) {
        if (event.getItem().getType().name().contains("SKULL") && NBTTagUtil.getIntData(event.getItem(), "coins") != 0) {
            if (BagCoinsUtil.addCoins(event.getDestination(), NBTTagUtil.getIntData(event.getItem(), "coins"))) event.getItem().setType(Material.AIR); event.getItem().setAmount(0);
        }
    }
}
