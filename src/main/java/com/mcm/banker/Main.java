package com.mcm.banker;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    public static Main instance;
    public static Plugin plugin;

    public static Main getInstance() {
        return Main.instance;
    }

    @Override
    public void onEnable() {
        Main.instance = this;
        Main.plugin = this;
        RegisterListeners.register();
    }

    @Override
    public void onDisable() {
        //-
    }
}
