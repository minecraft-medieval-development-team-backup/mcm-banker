package com.mcm.banker;

import com.mcm.banker.inventoryListeners.InventoryBanker;
import com.mcm.banker.listeners.ItemMoveOnInventoryEvent;
import com.mcm.banker.listeners.PickupItemEvent;
import com.mcm.banker.listeners.PlayerChatEvent;
import com.mcm.banker.listeners.PlayerInteractBanker;
import net.citizensnpcs.api.CitizensAPI;
import org.bukkit.Bukkit;

public class RegisterListeners {

    public static void register() {
        CitizensAPI.registerEvents(new PlayerInteractBanker());
        Bukkit.getPluginManager().registerEvents(new InventoryBanker(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PlayerChatEvent(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new PickupItemEvent(), Main.plugin);
        Bukkit.getPluginManager().registerEvents(new ItemMoveOnInventoryEvent(), Main.plugin);
    }
}
